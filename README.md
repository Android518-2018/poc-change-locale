# Proof of Concept 
## Change Locale on Android
User beware this code is very basic and has not been
thoroughly tested.  

The language is changed to french when the app is started.

It allows for changes of language
via  buttons between french, english, spanish, german, italian.

You may want to play around with changing the country also.